-module(tlight_lib_SUITE).
-compile(export_all).
-include_lib("common_test/include/ct.hrl").

-include("sequence.hrl").
-include("digit_macros.hrl").


%%%===================================================================
%%% Common Test callbacks
%%%===================================================================
all() ->
    [fihished_error_test, first_red_error_test, example_test, broke_sections_test, observation_hard_test, observation_eighty_test, observation_eighty_eight_test, full_number_info_test, is_match_number_test, observation_eighty_eight_center_test, observation_simple_test, possible_start_test, is_possible_test, is_possible_digit_test].

init_per_testcase(_TestCase, Config) ->
    Config.

end_per_testcase(_TestCase, _Config) ->
    ok.

break([], []) -> [];
break([_|Number], [1|Broken]) ->
  [0 | break(Number, Broken)];
break([X|Number], [0|Broken]) ->
  [X | break(Number, Broken)].

%%%===================================================================
%%% Test cases
%%%===================================================================
first_red_error_test(_Config) ->
  Sequence = tlight_lib:new_sequence(),
  {error, first_red} = tlight_lib:observation(Sequence, {red, [?ZERO, ?ZERO]}),
  ok.

fihished_error_test(_Config) ->
  Sequence = tlight_lib:new_sequence(),

  {ok, Sequence2} = tlight_lib:observation(Sequence, {green, [?ZERO,  ?ONE]}),
  {ok, Sequence3} = tlight_lib:observation(Sequence2, {red}),
  {error, finished} = tlight_lib:observation(Sequence3, {green, [?ZERO,  ?ONE]}),

  ok.

example_test(_Config) ->
  Sequence = tlight_lib:new_sequence(),

  {ok, Sequence2} = tlight_lib:observation(Sequence, {green, [[1,1,1,0,1,1,1],  [0,0,1,1,1,0,1]]}),
  [2, 8, 82, 88] = Sequence2#sequence.start,
  [_, [-1|_]] = Sequence2#sequence.numbers,

  {ok, Sequence3} = tlight_lib:observation(Sequence2, {green, [[1,1,1,0,1,1,1], [0,0,1,0,0,0,0]]}),
  [2, 8, 82, 88] = Sequence3#sequence.start,
  [_, [-1,_,_,_,_,-1,_]] = Sequence3#sequence.numbers,

  {ok, Sequence4} = tlight_lib:observation(Sequence3, {red}),

  [2] = Sequence4#sequence.start,

  ok.
broke_sections_test(_Config) ->
  [1,0,0,1,0,0,1] =break(?THREE, [0,1,1,0,1,1,0]),
  ok.

observation_hard_test(_Config) ->
  Sequence = tlight_lib:new_sequence(),

  Broken = fun(N)-> break(N, [0,1,1,0,1,1,0]) end,

  {ok, Sequence2} = tlight_lib:observation(Sequence, {green, [Broken(?THREE),Broken(?THREE)]}),
  {ok, Sequence3} = tlight_lib:observation(Sequence2, {green, [Broken(?THREE),Broken(?TWO)]}),
  {ok, Sequence4} = tlight_lib:observation(Sequence3, {green, [Broken(?THREE),Broken(?ONE)]}),
  {ok, Sequence5} = tlight_lib:observation(Sequence4, {green, [Broken(?THREE),Broken(?ZERO)]}),
  {ok, Sequence6} = tlight_lib:observation(Sequence5, {green, [Broken(?TWO),Broken(?NINE)]}),
  {ok, Sequence7} = tlight_lib:observation(Sequence6, {green, [Broken(?TWO),Broken(?EIGHT)]}),
  {ok, Sequence8} = tlight_lib:observation(Sequence7, {green, [Broken(?TWO),Broken(?SEVEN)]}),
  3 = length(Sequence8#sequence.start),

  {ok, Sequence9} = tlight_lib:observation(Sequence8, {green, [Broken(?TWO),Broken(?SIX)]}),
  {ok, Sequence10} = tlight_lib:observation(Sequence9, {green, [Broken(?TWO),Broken(?FIFE)]}),
  {ok, Sequence11} = tlight_lib:observation(Sequence10, {green, [Broken(?TWO),Broken(?FOUR)]}),
  {ok, Sequence12} = tlight_lib:observation(Sequence11, {green, [Broken(?TWO),Broken(?THREE)]}),
  {ok, Sequence13} = tlight_lib:observation(Sequence12, {green, [Broken(?TWO),Broken(?TWO)]}),
  {ok, Sequence14} = tlight_lib:observation(Sequence13, {green, [Broken(?TWO),Broken(?ONE)]}),
  {ok, Sequence15} = tlight_lib:observation(Sequence14, {green, [Broken(?TWO),Broken(?ZERO)]}),
  3 = length(Sequence15#sequence.start),

  {ok, Sequence16} = tlight_lib:observation(Sequence15, {green, [Broken(?ONE),Broken(?NINE)]}),
  1 = length(Sequence16#sequence.start),
  ok.

observation_simple_test(_Config) ->
  Sequence = tlight_lib:new_sequence(),

  {ok, Sequence2} = tlight_lib:observation(Sequence, {green, [?ONE,?ZERO]}),
  14 = length(Sequence2#sequence.start),

  {ok, Sequence3} = tlight_lib:observation(Sequence2, {green, [?ZERO, ?NINE]}),
  2 = length(Sequence3#sequence.start),

  {ok, Sequence4} = tlight_lib:observation(Sequence3, {green, [?ZERO, ?EIGHT]}),
  2 = length(Sequence4#sequence.start),

  ok.

observation_eighty_eight_center_test(_Config) ->
  Sequence = tlight_lib:new_sequence(),

  {ok, Sequence2} = tlight_lib:observation(Sequence, {green, [?EIGHT,?ZERO]}),
  {ok, Sequence3} = tlight_lib:observation(Sequence2, {green, [?EIGHT,?SEVEN]}),

  1 = length(Sequence3#sequence.start),
  ok.

observation_eighty_test(_Config) ->
  Sequence = tlight_lib:new_sequence(),

  {ok, Sequence2} = tlight_lib:observation(Sequence, {green, [?EIGHT,?ZERO]}),
  {ok, Sequence3} = tlight_lib:observation(Sequence2, {green, [?SEVEN,?NINE]}),

  1 = length(Sequence3#sequence.start),
  ok.


observation_eighty_eight_test(_Config) ->
  Sequence = tlight_lib:new_sequence(),

  {ok, Sequence2} = tlight_lib:observation(Sequence, {green, [?EIGHT,?EIGHT]}),
  1 = length(Sequence2#sequence.start),
  ok.

full_number_info_test(_Config) ->
  Broken = fun(N)-> break(N, [0,1,1,0,1,1,0]) end,

  Start = [33, 88],
  [N1, N2] = tlight_lib:full_numbers_info([Broken(?THREE), Broken(?TWO)], [[1,0,0,0,0,0,0], [1,0,0,0,0,0,0]], Start, 1),
  [1,0,-1,1,0,-1,1] = N1,
  [1,0,-1,1,0,0,1] = N2,
  ok.

possible_start_test(_Config) ->
  [88] = tlight_lib:posible_start([[1,1,1,1,1,1,1], [1,1,1,1,1,1,1]]),
  4 = length(tlight_lib:posible_start([[1,1,1,0,1,1,1], [1,1,1,0,1,1,1]])),
  100 = length(tlight_lib:posible_start([[0,0,0,0,0,0,0], [0,0,0,0,0,0,0]])),
  10 = length(tlight_lib:posible_start([[1,1,1,1,1,1,1], [0,0,0,0,0,0,0]])),
  20 = length(tlight_lib:posible_start([[1,1,1,0,1,1,1], [0,0,0,0,0,0,0]])),
  14 = length(tlight_lib:posible_start([[1,1,1,0,1,1,1], [0,0,0,1,0,0,0]])),
  ok.
is_possible_test(_Config) ->
  Info = [[0,0,0,0,0,0,0],[0,0,0,0,0,0,0]],
  true = tlight_lib:is_possible(66, [[1,1,0,1,0,1,1], [1,1,0,1,1,1,1]], Info),
  true = tlight_lib:is_possible(1, [[1,1,0,0,0,1,1], [0,0,1,0,0,0,0]], Info),
  true = tlight_lib:is_possible(91, [[1,1,0,1,0,1,1], [0,0,1,0,0,1,0]], Info),
  false = tlight_lib:is_possible(7, [[1,1,0,1,0,1,1], [0,0,1,0,0,1,0]], Info),
  false = tlight_lib:is_possible(77, [[1,1,0,1,0,1,1], [0,0,1,0,0,1,0]], Info),
  false = tlight_lib:is_possible(79, [[1,0,0,0,0,0,0], [0,0,0,0,1,0,1]], Info),
  ok.
is_possible_digit_test(_Config) ->
  Info = [0,0,0,0,0,0,0],
  true = tlight_lib:is_possible_digit(0, [1,1,1,0,1,1,1], Info),
  true = tlight_lib:is_possible_digit(1, [0,0,1,0,0,0,0], Info),
  true = tlight_lib:is_possible_digit(1, [0,0,0,0,0,1,0], Info),
  true = tlight_lib:is_possible_digit(2, [1,0,1,1,1,0,1], Info),
  true = tlight_lib:is_possible_digit(3, [1,0,1,1,0,1,1], Info),
  true = tlight_lib:is_possible_digit(4, [0,1,1,1,0,1,0], Info),
  true = tlight_lib:is_possible_digit(5, [1,0,0,1,0,1,1], Info),
  true = tlight_lib:is_possible_digit(6, [1,1,0,1,1,1,1], Info),
  true = tlight_lib:is_possible_digit(7, [1,0,1,0,0,1,0], Info),
  true = tlight_lib:is_possible_digit(8, [1,1,1,1,1,1,1], Info),
  true = tlight_lib:is_possible_digit(8, [0,1,0,1,0,1,1], Info),
  true = tlight_lib:is_possible_digit(9, [1,1,1,1,0,1,1], Info),

  false = tlight_lib:is_possible_digit(0, [0,0,0,1,0,0,0], Info),
  false = tlight_lib:is_possible_digit(1, [1,0,0,0,0,0,0], Info),
  false = tlight_lib:is_possible_digit(1, [0,0,0,0,1,0,0], Info),
  false = tlight_lib:is_possible_digit(2, [0,1,0,0,0,0,0], Info),
  false = tlight_lib:is_possible_digit(2, [0,0,0,0,0,1,0], Info),
  false = tlight_lib:is_possible_digit(3, [0,1,0,0,0,0,0], Info),
  false = tlight_lib:is_possible_digit(3, [0,0,0,0,1,0,0], Info),
  false = tlight_lib:is_possible_digit(4, [1,0,0,0,0,0,0], Info),
  false = tlight_lib:is_possible_digit(4, [0,0,0,0,1,0,0], Info),
  false = tlight_lib:is_possible_digit(5, [0,0,1,0,0,0,0], Info),
  false = tlight_lib:is_possible_digit(5, [0,0,1,0,0,0,0], Info),
  false = tlight_lib:is_possible_digit(6, [0,0,1,0,0,0,0], Info),
  false = tlight_lib:is_possible_digit(7, [0,1,0,0,0,0,0], Info),
  false = tlight_lib:is_possible_digit(7, [0,0,0,1,0,0,0], Info),
  false = tlight_lib:is_possible_digit(9, [0,0,0,0,1,0,0], Info),
  ok.

is_match_number_test(_Config) ->
  true = tlight_lib:is_match_number(?ONE, [0,0,0,0,0,0,0], [0,0,0,0,0,0,0]),
  true = tlight_lib:is_match_number(?ONE, [0,0,1,0,0,0,0], [0,0,1,0,0,0,0]),
  false = tlight_lib:is_match_number(?ONE, [0,0,0,0,0,1,0], [0,0,1,0,0,0,0]),
  false = tlight_lib:is_match_number(?EIGHT, [0,0,0,0,0,0,0], [0,0,1,0,0,0,0]),
  false = tlight_lib:is_match_number(?EIGHT, [1,1,1,0,1,1,1], [1,0,0,1,0,0,0]),
  false = tlight_lib:is_match_number(?ZERO, [1,1,1,0,1,1,0], [1,0,0,1,0,0,1]),
  true = tlight_lib:is_match_number(?ZERO, [1,1,1,0,1,1,0], [1,0,0,1,0,0,0]),
  true = tlight_lib:is_match_number(?SEVEN, [0,0,0,0,0,0,0], [0,1,0,1,1,0,1]),
  false = tlight_lib:is_match_number(?SIX, [1,0,0,1,1,1,1], [0,1,0,1,1,0,1]),
  true = tlight_lib:is_match_number(?SIX, [1,1,0,1,1,1,1], [0,1,0,1,1,0,1]),
  ok.

full_numbers_info_final_test(_Config) ->
  [[-1,-1,-1,0,-1,-1,-1], [-1, 0,-1,-1,-1,-1,-1]] = tlight_lib:full_numbers_info_final([?NONUM, ?NONUM], 2),
  [[-1,-1,-1,0,-1,-1,-1], [-1, -1,-1,-1,-1,-1,-1]] = tlight_lib:full_numbers_info_final([?NONUM, ?NONUM], 8),
  [[-1,-1,-1,0,-1,-1,-1], [-1, -1,-1,-1,-1,-1,-1]] = tlight_lib:full_numbers_info_final([?NONUM, ?NONUM], 10),
  [[1,1,1,-1,1,1,1], [-1, -1,1,-1,-1,1,-1]] = tlight_lib:full_numbers_info_final([?ZERO, ?ONE], 20),
  [?EIGHT, [0, 0,-1,0,0,-1,0]] = tlight_lib:full_numbers_info_final([?EIGHT, ?NONUM], 1),
  ok.

fail_double_test(_Config) ->
  Sequence = tlight_lib:new_sequence(),

  {ok, Sequence2} = tlight_lib:observation(Sequence, {green, [[1,1,1,0,1,1,1], [0,0,1,0,0,0,0]]}),
  {ok, Sequence3} = tlight_lib:observation(Sequence2, {green, [[1,1,1,0,1,1,1], [0,0,1,0,0,0,0]]}),

  {ok, _} = tlight_lib:observation(Sequence3, {red}),

  ok.
