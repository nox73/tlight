-module(tlight_store_SUITE).
-compile(export_all).
-include_lib("common_test/include/ct.hrl").

all() ->
    [simple_store_test].

init_per_testcase(_TestCase, Config) ->
  application:set_env(tlight, store, ets),
  Config.

end_per_testcase(_TestCase, _Config) ->
    ok.

simple_store_test(_Config) ->

  tlight_store:init(),

  Uid = uid,
  Val = val,

  {error, not_found} = tlight_store:get(<<"Not found uid">>),
  {ok} = tlight_store:set(Uid, Val),
  {ok, Val} = tlight_store:get(Uid),
  {ok} = tlight_store:clear(),
  {error, not_found} = tlight_store:get(Uid),

  ok.

