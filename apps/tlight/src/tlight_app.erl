-module(tlight_app).

-behaviour(application).

%% Application callbacks
-export([start/2, stop/1]).

%% ===================================================================
%% Application callbacks
%% ===================================================================

start(_StartType, _StartArgs) ->
    Dispatch = cowboy_router:compile([
        {'_', [
            {"/clear", clear_handler, []},
            {"/sequence/create", sequence_handler, []},
            {"/observation/add", observation_handler, []}
        ]}
    ]),

    {ok, Port } = application:get_env(port),
    {ok, _} = cowboy:start_http(http_listener, 5,
        [{port, Port}],
        [{env, [{dispatch, Dispatch}]}]
    ),

    tlight_sup:start_link().

stop(_State) ->
    ok.
