-module(tlight_store).

-export([init/0, set/2, get/1, clear/0]).

init() ->
  {ok, Store} = application:get_env(tlight, store),
  init(Store).
init(ets) ->
  ets:new(sequences, [public, set, named_table, {write_concurrency, true}, {read_concurrency, true}]);
init(dets) ->
  {ok, Path} = application:get_env(tlight, file),
  dets:open_file(sequences, [{file, Path}, {type, set}, {auto_save, 2000}]).

set(Uuid, Val) ->
  {ok, Store} = application:get_env(tlight, store),
  set(Uuid, Val, Store).
set(Uuid, Val, ets) ->
  ets:insert(sequences,  {Uuid, Val}),
  {ok};
set(Uuid, Val, dets) ->
  dets:insert(sequences,  {Uuid, Val}),
  {ok}.

get(Uuid) ->
  {ok, Store} = application:get_env(tlight, store),
  get(Uuid, Store).
get(Uuid, ets) ->
  case ets:lookup(sequences, Uuid) of
    [{_, Val}] -> {ok, Val};
    _ -> {error, not_found}
  end;
get(Uuid, dets) ->
  case dets:lookup(sequences, Uuid) of
    [{_, Val}] -> {ok, Val};
    _ -> {error, not_found}
  end.



clear() ->
  {ok, Store} = application:get_env(tlight, store),
  clear(Store).
clear(ets) ->
  ets:delete_all_objects(sequences),
  {ok};
clear(dets) ->
  dets:delete_all_objects(sequences),
  {ok}.
