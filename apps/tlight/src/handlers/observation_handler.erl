-module(observation_handler).

-include("sequence.hrl").

-export([init/2]).
-export([allowed_methods/2]).
-export([content_types_accepted/2]).
-export([resource_exists/2]).

% content funs
-export([create/2]).

%%
%% Cowboy REST callbacks
init(Req, Opts) ->
    {cowboy_rest, Req, Opts}.

allowed_methods(Req, Context) ->
    {[<<"POST">>], Req, Context}.

content_types_accepted(Req, Context) ->
      CTA = [{{<<"application">>, <<"json">>, '*'}, create}],
      {CTA, Req, Context}.

resource_exists(Req, State) ->
      {false, Req, State}.

create(Req0, Context) ->
  {ok, Body, Req2} = cowboy_req:body(Req0),
  case from_json(jiffy:decode(Body)) of
   {Uid, Color, Numbers} ->
      Resp = tlight_srv:add_observation(Uid, {Color, Numbers}),
      return_response(Resp, Req2, Context);
    Error -> return_response(Error, Req2, Context)
  end.


return_response({error, no_solution}, Req, Context) ->
  return_error(422, <<"No solutions found">>, Req, Context);
return_response({error, validation_fail}, Req, Context) ->
  return_error(422, <<"Validation fail">>, Req, Context);
return_response({error, finished}, Req, Context) ->
  return_error(422, <<"The red observation should be the last">>, Req, Context);
return_response({error, first_red}, Req, Context) ->
  return_error(422, <<"There isn't enough data">>, Req, Context);
return_response({error, not_found}, Req, Context) ->
  return_error(404, <<"The sequence isn't found">>, Req, Context);
return_response({ok, Sequence}, Req, Context) ->
  Seq = sequence_dump(Sequence),

  Json = jiffy:encode({[{status, ok}, {response, {Seq}}]}),

  ReqLast = cowboy_req:set_resp_body(Json, Req),
  {true, ReqLast, Context}.

return_error(Status, Msg, Req, Context) ->
  Json = jiffy:encode({[{status, error}, {msg, Msg}]}),
  Req2 = cowboy_req:reply(Status, [], Json, Req),
  {stop, Req2, Context}.

sequence_dump(#sequence{start=Start,numbers=[N1, N2]}) ->
  [{start, Start},{missing, [sections_to_binary(N1), sections_to_binary(N2)]}].

from_json({Json}) ->
  Uid = proplists:get_value(<<"sequence">>, Json, undefined),
  {Observation} = proplists:get_value(<<"observation">>, Json, {[]}),

  Color = case proplists:get_value(<<"color">>, Observation) of
                <<"green">> -> green;
                <<"red">> -> red;
                _ -> undefined
             end,

  N = case proplists:get_value(<<"numbers">>, Observation) of
                NS=[_, _] -> lists:map(fun sections_to_list/1, NS);
                _ -> [[], []]
              end,

  case is_valid(Uid, Color, N) of
    true -> {Uid, Color, N};
    _ -> {error, validation_fail}
  end.


is_valid(undefined, _, _) -> false;
is_valid(_, green, [[_,_,_,_,_,_,_], [_,_,_,_,_,_,_]]) -> true;
is_valid(_, red, _) -> true;
is_valid(_, _, _) -> false.

sections_to_binary(N) ->
  list_to_binary(lists:map(fun section_to_binary/1, N)).
section_to_binary(-1) -> 49;
section_to_binary(_) -> 48.

sections_to_list(Str) when is_bitstring(Str) ->
  lists:map(fun section_to_int/1, binary_to_list(Str));
sections_to_list(_) -> [].
section_to_int(49) -> 1;
section_to_int(48) -> 0;
section_to_int(_) -> 0.

