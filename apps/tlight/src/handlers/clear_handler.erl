-module(clear_handler).

-export([init/2]).
-export([allowed_methods/2]).
-export([content_types_provided/2]).
% -export([content_types_accepted/2]).

% content funs
-export([display/2]).

%%
%% Cowboy REST callbacks
init(Req, Opts) ->
    {cowboy_rest, Req, Opts}.

allowed_methods(Req, Context) ->
    {[<<"GET">>], Req, Context}.

content_types_provided(Req, Context) ->
    CTA = [{{<<"application">>, <<"json">>, '*'}, display}],
    {CTA, Req, Context}.

display(Req0, Context) ->
    tlight_srv:clear(),
    Json = jiffy:encode({[{status, ok}, {response, ok}]}),
    {Json, Req0, Context}.
