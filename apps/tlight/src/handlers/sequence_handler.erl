-module(sequence_handler).

-export([init/2]).
-export([allowed_methods/2]).
-export([content_types_accepted/2]).
-export([resource_exists/2]).

% content funs
-export([create/2]).

%%
%% Cowboy REST callbacks
init(Req, Opts) ->
    {cowboy_rest, Req, Opts}.

allowed_methods(Req, Context) ->
    {[<<"POST">>], Req, Context}.

content_types_accepted(Req, Context) ->
      CTA = [{{<<"application">>, <<"json">>, '*'}, create}],
      {CTA, Req, Context}.

resource_exists(Req, State) ->
      {false, Req, State}.

create(Req0, Context) ->
  {ok, Uid} = tlight_srv:create_sequence(),
  Json = jiffy:encode({[{status, ok}, {response, {[{sequence, Uid}]}}]}),
  Req2 = cowboy_req:set_resp_body(Json, Req0),
  {true, Req2, Context}.
