-module(tlight_lib).

-export([new_sequence/0,observation/2]).

%-ifdef(TEST).
-export([is_possible/3, is_possible_digit/3, posible_start/1, is_match_number/3, full_numbers_info/4, full_numbers_info_final/2]).
%-endif.

-include("sequence.hrl").
-include("digit_macros.hrl").

new_sequence() ->
  #sequence{}.

observation(#sequence{start=[], observation=ONumber}, _) when ONumber > 0  ->
  {error, no_solution};
observation(#sequence{observation=0}, {red, _}) ->
  {error, first_red};
observation(#sequence{finished=true}, _) ->
  {error, finished};
observation(Sequence, {red, _}) ->
  observation(Sequence, {red});
observation(Sequence=#sequence{start=OldStart, numbers=NumberInfo, observation=ONumber}, {red}) ->
  case element_in_list(ONumber, OldStart) of
    true ->
      Start = [ONumber],
      NumbersInfo2 = full_numbers_info_final(NumberInfo, ONumber),
      Sequence2 = Sequence#sequence{start=Start, finished=true, numbers=NumbersInfo2},
      {ok, Sequence2};
    false ->
      Sequence2 = Sequence#sequence{start=[], finished=true},
      validate_solution(Sequence2)
  end;
observation(Sequence=#sequence{observation=0, numbers=NumberInfo, finished=false}, {green, Numbers}) ->
  Start = posible_start(Numbers),
  NumbersInfo2 = full_numbers_info(NumberInfo, Numbers, Start, 0),
  Sequence2 = Sequence#sequence{observation=1, start=Start, numbers=NumbersInfo2},
  validate_solution(Sequence2);
observation(Sequence=#sequence{observation=ONumber, start=Start, numbers=NumbersInfo, finished=false}, {green, Numbers}) ->
  NumbersInfo2 = full_numbers_info(NumbersInfo, Numbers, Start, ONumber),
  Start2 = [X || X <- Start, X - ONumber >= 0, is_possible(X - ONumber, Numbers, NumbersInfo2)],
  Sequence2 = Sequence#sequence{start=Start2, observation=ONumber+1, numbers=NumbersInfo2},
  validate_solution(Sequence2).

validate_solution(Sequence=#sequence{start=[]}) ->
  {error, no_solution, Sequence};
validate_solution(Sequence) ->
  {ok, Sequence}.

full_numbers_info_final([N1, N2], Start) when Start > 15 ->
  [full_info_zero(N1), full_info_zero(N2)];
full_numbers_info_final([N1, N2], Start) when Start > 5 ->
  X1 = trunc(Start / 10),
  [full_numbers_info_final_digit(N1, X1, 0, true), full_info_zero(N2)];
full_numbers_info_final([N1, N2], Start) ->
  X2 = Start rem 10,
  X1 = trunc(Start / 10),
  [full_numbers_info_final_digit(N1, X1, 0, true), full_numbers_info_final_digit(N2, X2, 0, false)].

full_numbers_info_final_digit([], _, _, _) -> [];
full_numbers_info_final_digit([0|N], X, SectionNumber, S) ->
  Val = case section_in_digit_final(SectionNumber, X, S) of
    1 -> -1;
    _ -> 0
  end,
  [Val | full_numbers_info_final_digit(N, X, SectionNumber + 1, S)];
full_numbers_info_final_digit([Val|N], X, SectionNumber, S) ->
  [Val |full_numbers_info_final_digit(N, X, SectionNumber + 1, S)].

full_info_zero([]) -> [];
full_info_zero([0|N]) -> [-1|full_info_zero(N)];
full_info_zero([V|N]) -> [V|full_info_zero(N)].

section_in_digit_final(_, 0, false) -> 0;
section_in_digit_final(_, -1, true) -> 0;
section_in_digit_final(Section, Digit, S) ->
  case section_in_digit(Section, Digit) of
    1 -> 1;
    0 -> section_in_digit_final(Section, Digit - 1, S)
  end.


full_numbers_info([NInfo1, NInfo2], [NCur1, NCur2], Start, Observation) ->
  Posible1 = lists:usort([trunc((X - Observation)/10) || X <- Start, X - Observation >= 0]),
  Posible2 = lists:usort([((X - Observation) rem 10) || X <- Start, X - Observation >= 0]),
  [full_number_info(NInfo1, NCur1, Posible1), full_number_info(NInfo2, NCur2, Posible2)].

full_number_info(NInfo, NCur, Posible) ->
  full_number_info(NInfo, NCur, 0, Posible).
full_number_info([], [], 7, _) -> [];
full_number_info([X| NInfo], [Y| NCur], Section, Posible) ->
  Res = case full_number_info_digit(X, Y) of
          0 -> check_broken_section(Section, Posible);
          Val -> Val
        end,
  [Res | full_number_info(NInfo, NCur, Section + 1, Posible)].

full_number_info_digit(-1, _) -> -1;
full_number_info_digit(1, _) -> 1;
full_number_info_digit(_, 1) -> 1;
full_number_info_digit(0, 0) -> 0.

check_broken_section(_, []) -> -1;
check_broken_section(Section, [Digit | Posible]) ->
  case section_in_digit(Section, Digit) of
    1 -> check_broken_section(Section, Posible);
    0 -> 0
  end.

section_in_digit(Section, Digit) ->
  N = Section + 1,
  case Digit of
    0 -> lists:nth(N, ?ZERO);
    1 -> lists:nth(N, ?ONE);
    2 -> lists:nth(N, ?TWO);
    3 -> lists:nth(N, ?THREE);
    4 -> lists:nth(N, ?FIFE);
    5 -> lists:nth(N, ?FOUR);
    6 -> lists:nth(N, ?SIX);
    7 -> lists:nth(N, ?SEVEN);
    8 -> lists:nth(N, ?EIGHT);
    9 -> lists:nth(N, ?NINE)
  end.

posible_start([N1, N2]) ->
  Info = [0,0,0,0,0,0,0],
  Posible1 = [X1 || X1 <- lists:seq(0, 9), is_possible_digit(X1, N1, Info)],
  Posible2 = [X2 || X2 <- lists:seq(0, 9), is_possible_digit(X2, N2, Info)],
  [Y1 * 10 + Y2 || Y1 <- Posible1, Y2 <- Posible2].

is_possible(X, [N1, N2], [Info1, Info2]) ->
  X2 = X rem 10,
  X1 = trunc(X / 10),
  is_possible_digit(X1, N1, Info1) and is_possible_digit(X2, N2, Info2).

is_possible_digit(0, Number, Info) ->
  is_match_number(?ZERO, Number, Info);
is_possible_digit(1, Number, Info) ->
  is_match_number(?ONE, Number, Info);
is_possible_digit(2, Number, Info) ->
  is_match_number(?TWO, Number, Info);
is_possible_digit(3, Number, Info) ->
  is_match_number(?THREE, Number, Info);
is_possible_digit(4, Number, Info) ->
  is_match_number(?FOUR, Number, Info);
is_possible_digit(5, Number, Info) ->
  is_match_number(?FIFE, Number, Info);
is_possible_digit(6, Number, Info) ->
  is_match_number(?SIX, Number, Info);
is_possible_digit(7, Number, Info) ->
  is_match_number(?SEVEN, Number, Info);
is_possible_digit(8, Number, Info) ->
  is_match_number(?EIGHT, Number, Info);
is_possible_digit(9, Number, Info) ->
  is_match_number(?NINE, Number, Info).

is_match_number([], [], []) ->
  true;
is_match_number([0|_], [1|_], _) ->
  false;
is_match_number([1|_], [0|_], [1|_]) ->
  false;
is_match_number([_|Ideal], [_|Number], [_|Info]) ->
  is_match_number(Ideal, Number, Info).

element_in_list(_, []) -> false;
element_in_list(Element, [Element| _]) -> true;
element_in_list(Element, [_| List]) -> element_in_list(Element, List).
