-module(tlight_srv).

-export([start/0, create_sequence/0, add_observation/2, clear/0]).

start() ->
  tlight_store:init().

create_sequence() ->
  Uuid = list_to_binary(uuid:to_string(uuid:uuid4())),
  NewSequence = tlight_lib:new_sequence(),
  tlight_store:set(Uuid, NewSequence),
  {ok, Uuid}.

add_observation(Uuid, Observation) ->
  case tlight_store:get(Uuid) of
    {ok, Sequence} ->

     case tlight_lib:observation(Sequence, Observation) of
       {ok, Sequence2} ->
         tlight_store:set(Uuid, Sequence2),
         {ok, Sequence2};
       {error, Error, Sequence2} ->
         tlight_store:set(Uuid, Sequence2),
         {error, Error};
       Error -> Error
     end;

    Error -> Error
  end.

clear() ->
  tlight_store:clear().
