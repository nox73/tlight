-module(tlight).

-export([start/0, stop/0, restart/0]).

applications() ->
  [crypto, ranch, cowlib, cowboy, tlight].

start() ->
  ok = lager:start(),
  ok = sync:go(),
  [ok = application:start(App) || App <- applications()].

stop() ->
  [ok = application:stop(App) || App <- applications()].

restart() ->
  stop(),
  start().
