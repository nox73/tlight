## Traffic Light
### Запуск при помощи Docker
```        
make start
```
Запуск тестов
```        
make start_test
```
###### Зависимости
- Docker version 1.3.2

### Без Docker

Указать в `sys.config` ключ `/tlight/file` путь к файлу, где будет лежать файл с данными.

```
make all
make run
```
Запуск тестов
```
make test
```
###### Зависимости
- Erlang 17

## Использование
```
curl -X POST -H "Content-Type: application/json" http://localhost:8080/sequence/create                                     

{"status":"ok","response":{"sequence":"b688e8f1-e0bc-45a0-8f5b-a0f37140e5e0"}

curl -X POST -H "Content-Type: application/json" http://localhost:8080/observation/add -d '{"sequence":"b688e8f1-e0bc-45a0-8f5b-a0f37140e5e0","observation":{"color":"green", "numbers":["1110111","0011101"]}}'

{"status":"ok","response":{"start":[2,8,82,88],"missing":["0000000","1000000"]}}

curl -X POST -H "Content-Type: application/json" http://localhost:8080/observation/add -d '{"sequence":"b688e8f1-e0bc-45a0-8f5b-a0f37140e5e0","observation":{"color":"green", "numbers":["1110111","0010000"]}}'

{"status":"ok","response":{"start":[2,8,82,88],"missing":["0000000","1000010"]}}

curl -X POST -H "Content-Type: application/json" http://localhost:8080/observation/add -d '{"sequence":"b688e8f1-e0bc-45a0-8f5b-a0f37140e5e0","observation":{"color":"red"}}'

{"status":"ok","response":{"start":[2],"missing":["0000000","1000010"]}}
```
#### Обработка ошибок
```
curl -X POST -H "Content-Type: application/json" http://localhost:8080/observation/add -d '{"sequence":"b688e8f1-e0bc-45a0-8f5b-a0f37140e5e0","observation":{"color":"green", "numbers":["1110111","0010000"]}}'

{"status":"error","msg":"The red observation should be the last"}

...

curl -X POST -H "Content-Type: application/json" http://localhost:8080/observation/add -d '{"sequence":"151cd706-abdc-4799-acb7-b9fdab5f681e","observation":{"color":"red"}}'

{"status":"error","msg":"There isn't enough data"}

curl -X POST -H "Content-Type: application/json" http://localhost:8080/observation/add -d '{"sequence":"asdf","observation":{"color":"red"}}'

{"status":"error","msg":"The sequence isn't found"}

...

curl -X POST -H "Content-Type: application/json" http://localhost:8080/observation/add -d '{"sequence":"d0bbbeef-68e1-4913-b8e3-16edbbfd0ef1","observation":{"color":"green", "numbers":["111","001"]}}'

{"status":"error","msg":"Validation fail"}
```
