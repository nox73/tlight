FROM nox73/erlang

RUN mkdir /tmp/tlight/

WORKDIR /usr/src/app

ADD ./rebar.config /usr/src/app/rebar.config
RUN rebar get-deps

ADD ./ /usr/src/app

RUN make all

CMD make run
