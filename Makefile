PROJECT := tlight
REBAR = rebar
mkfile_path := $(abspath $(lastword $(MAKEFILE_LIST)))
mkfile_dir := $(dir $(mkfile_path))

all: clean get-deps compile

clean:
	@$(REBAR) clean

start:
	docker run -ti --rm -v /tmp/tlight:/tmp/tlight -p 8080:8080 nox73/tlight

start_test:
	docker run -ti --rm nox73/tlight make test

compile:
	@$(REBAR) compile

cleanapp:
	@($(REBAR) clean skip_deps=true)

compileapp: cleanapp
	@($(REBAR) compile skip_deps=true)

get-deps:
	@$(REBAR) get-deps

run: compileapp
	@(erl -config $(CURDIR)/sys -pa apps/*/ebin deps/*/ebin -boot start_sasl -s $(PROJECT))

dev:
	docker run -ti --rm -v /tmp/tlight:/tmp/tlight -w /usr/src/app -p 8080:8080 -v $(mkfile_dir):/usr/src/app/ nox73/erlang /bin/bash -l

test: compileapp
	cd apps/tlight && rebar ct

.PHONY: all clean compile run dev start
